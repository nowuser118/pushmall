package co.pushmall.mp.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.mp.domain.PushMallArticle;
import co.pushmall.mp.service.dto.PushMallArticleDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-10-07
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallArticleMapper extends EntityMapper<PushMallArticleDTO, PushMallArticle> {

}
