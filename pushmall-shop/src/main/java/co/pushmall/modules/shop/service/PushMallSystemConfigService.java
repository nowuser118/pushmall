package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallSystemConfig;
import co.pushmall.modules.shop.service.dto.PushMallSystemConfigDTO;
import co.pushmall.modules.shop.service.dto.PushMallSystemConfigQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-10-10
 */
//@CacheConfig(cacheNames = "yxSystemConfig")
public interface PushMallSystemConfigService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallSystemConfigQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallSystemConfigDTO> queryAll(PushMallSystemConfigQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallSystemConfigDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallSystemConfigDTO create(PushMallSystemConfig resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallSystemConfig resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);

    PushMallSystemConfig findByKey(String str);
}
