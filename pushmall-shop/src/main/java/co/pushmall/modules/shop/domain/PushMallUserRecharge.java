package co.pushmall.modules.shop.domain;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author pushmall
 * @date 2020-03-02
 */
@Entity
@Data
@Table(name = "pushmall_user_recharge")
public class PushMallUserRecharge implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /**
     * 充值用户UID
     */
    @Column(name = "uid")
    private Integer uid;

    /**
     * 订单号
     */
    @Column(name = "order_id", unique = true)
    private String orderId;

    /**
     * 充值金额
     */
    @Column(name = "price")
    private BigDecimal price;

    /**
     * 充值类型
     */
    @Column(name = "recharge_type")
    private String rechargeType;

    /**
     * 是否充值
     */
    @Column(name = "paid")
    private Integer paid;

    /**
     * 充值支付时间
     */
    @Column(name = "pay_time")
    private Integer payTime;

    /**
     * 充值时间
     */
    @Column(name = "add_time")
    private Integer addTime;

    /**
     * 退款金额
     */
    @Column(name = "refund_price")
    private BigDecimal refundPrice;

    /**
     * 昵称
     */
    @Column(name = "nickname")
    private String nickname;

    public void copy(PushMallUserRecharge source) {
        BeanUtil.copyProperties(source, this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
