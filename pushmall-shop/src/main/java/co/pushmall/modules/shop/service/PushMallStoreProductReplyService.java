package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallStoreProductReply;
import co.pushmall.modules.shop.service.dto.PushMallStoreProductReplyDTO;
import co.pushmall.modules.shop.service.dto.PushMallStoreProductReplyQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-11-03
 */
//@CacheConfig(cacheNames = "yxStoreProductReply")
public interface PushMallStoreProductReplyService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStoreProductReplyQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStoreProductReplyDTO> queryAll(PushMallStoreProductReplyQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStoreProductReplyDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStoreProductReplyDTO create(PushMallStoreProductReply resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStoreProductReply resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
