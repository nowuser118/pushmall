package co.pushmall.modules.shop.domain;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author pushmall
 * @date 2019-10-13
 */
@Entity
@Data
@Table(name = "pushmall_store_product_attr_value")
public class PushMallStoreProductAttrValue implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    // 商品ID
    @Column(name = "product_id", nullable = false)
    private Integer productId;

    // 商品属性索引值 (attr_value|attr_value[|....])
    @Column(name = "suk", nullable = false)
    private String suk;

    // 属性对应的库存
    @Column(name = "stock", nullable = false)
    private Integer stock;

    // 销量
    @Column(name = "sales", nullable = false)
    private Integer sales;

    // 属性金额
    @Column(name = "price", nullable = false)
    private BigDecimal price;

    // 图片
    @Column(name = "image")
    private String image;

    // 唯一值
    @Column(name = "`unique`", nullable = false)
    private String unique;

    // 成本价
    @Column(name = "cost", nullable = false)
    private BigDecimal cost;

    // 批发价格
    @Column(name = "wholesale", nullable = false)
    private BigDecimal wholesale;

    // 商品编码
    @Column(name = "bar_code", nullable = false)
    private String barCode;

    // 包装规格
    @Column(name = "packaging", nullable = false)
    private String packaging;

    public void copy(PushMallStoreProductAttrValue source) {
        BeanUtil.copyProperties(source, this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
