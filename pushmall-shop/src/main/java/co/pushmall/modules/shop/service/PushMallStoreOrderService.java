package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallStoreOrder;
import co.pushmall.modules.shop.service.dto.OrderCountDto;
import co.pushmall.modules.shop.service.dto.OrderTimeDataDTO;
import co.pushmall.modules.shop.service.dto.PushMallStoreOrderDTO;
import co.pushmall.modules.shop.service.dto.PushMallStoreOrderQueryCriteria;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-10-14
 */
//@CacheConfig(cacheNames = "yxStoreOrder")
public interface PushMallStoreOrderService {

    OrderCountDto getOrderCount();

    OrderTimeDataDTO getOrderTimeData();

    Map<String, Object> chartCount();

    String orderType(int id, int pinkId, int combinationId, int seckillId, int bargainId, int shippingType);

    void refund(PushMallStoreOrder resources);

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStoreOrderQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStoreOrderDTO> queryAll(PushMallStoreOrderQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStoreOrderDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStoreOrderDTO create(PushMallStoreOrder resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStoreOrder resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);

    /**
     * 导出订单数据
     *
     * @param queryAll
     * @param response
     * @throws IOException
     * @throws ParseException
     */
    void download(List<PushMallStoreOrderDTO> queryAll, HttpServletResponse response) throws IOException, ParseException;

    List<PushMallStoreOrderDTO> findByIds(List<String> idList);
}
