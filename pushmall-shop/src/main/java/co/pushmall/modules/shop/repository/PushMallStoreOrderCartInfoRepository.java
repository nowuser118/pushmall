package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.StoreOrderCartInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author pushmall
 * @date 2019-10-14
 */
public interface PushMallStoreOrderCartInfoRepository extends JpaRepository<StoreOrderCartInfo, Integer>, JpaSpecificationExecutor {

    List<StoreOrderCartInfo> findByOid(int oid);

}
