package co.pushmall.modules.shop.service.impl;

import co.pushmall.modules.shop.domain.PushMallStoreOrderStatus;
import co.pushmall.modules.shop.repository.PushMallStoreOrderStatusRepository;
import co.pushmall.modules.shop.service.PushMallStoreOrderStatusService;
import co.pushmall.modules.shop.service.dto.PushMallStoreOrderStatusDTO;
import co.pushmall.modules.shop.service.dto.PushMallStoreOrderStatusQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallStoreOrderStatusMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-11-02
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStoreOrderStatusServiceImpl implements PushMallStoreOrderStatusService {

    private final PushMallStoreOrderStatusRepository pushMallStoreOrderStatusRepository;

    private final PushMallStoreOrderStatusMapper pushMallStoreOrderStatusMapper;

    public PushMallStoreOrderStatusServiceImpl(PushMallStoreOrderStatusRepository pushMallStoreOrderStatusRepository, PushMallStoreOrderStatusMapper pushMallStoreOrderStatusMapper) {
        this.pushMallStoreOrderStatusRepository = pushMallStoreOrderStatusRepository;
        this.pushMallStoreOrderStatusMapper = pushMallStoreOrderStatusMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallStoreOrderStatusQueryCriteria criteria, Pageable pageable) {
        Page<PushMallStoreOrderStatus> page = pushMallStoreOrderStatusRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallStoreOrderStatusMapper::toDto));
    }

    @Override
    public List<PushMallStoreOrderStatusDTO> queryAll(PushMallStoreOrderStatusQueryCriteria criteria) {
        return pushMallStoreOrderStatusMapper.toDto(pushMallStoreOrderStatusRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStoreOrderStatusDTO findById(Integer id) {
        Optional<PushMallStoreOrderStatus> yxStoreOrderStatus = pushMallStoreOrderStatusRepository.findById(id);
        //ValidationUtil.isNull(yxStoreOrderStatus,"PushMallStoreOrderStatus","id",id);
        if (yxStoreOrderStatus.isPresent()) {
            return pushMallStoreOrderStatusMapper.toDto(yxStoreOrderStatus.get());
        }
        return new PushMallStoreOrderStatusDTO();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStoreOrderStatusDTO create(PushMallStoreOrderStatus resources) {
        return pushMallStoreOrderStatusMapper.toDto(pushMallStoreOrderStatusRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStoreOrderStatus resources) {
        Optional<PushMallStoreOrderStatus> optionalPushMallStoreOrderStatus = pushMallStoreOrderStatusRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStoreOrderStatus, "PushMallStoreOrderStatus", "id", resources.getId());
        PushMallStoreOrderStatus pushMallStoreOrderStatus = optionalPushMallStoreOrderStatus.get();
        pushMallStoreOrderStatus.copy(resources);
        pushMallStoreOrderStatusRepository.save(pushMallStoreOrderStatus);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallStoreOrderStatusRepository.deleteById(id);
    }
}
