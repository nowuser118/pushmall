package co.pushmall.modules.shop.service.dto;

import lombok.Data;

/**
 * @author pushmall
 * @date 2019-10-10
 */
@Data
public class PushMallSystemConfigQueryCriteria {
}
