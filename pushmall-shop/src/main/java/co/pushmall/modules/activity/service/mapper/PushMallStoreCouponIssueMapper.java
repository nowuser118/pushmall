package co.pushmall.modules.activity.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.activity.domain.PushMallStoreCouponIssue;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponIssueDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-11-09
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallStoreCouponIssueMapper extends EntityMapper<PushMallStoreCouponIssueDTO, PushMallStoreCouponIssue> {

}
