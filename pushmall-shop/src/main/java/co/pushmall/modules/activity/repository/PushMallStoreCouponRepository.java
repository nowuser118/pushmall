package co.pushmall.modules.activity.repository;

import co.pushmall.modules.activity.domain.PushMallStoreCoupon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-11-09
 */
public interface PushMallStoreCouponRepository extends JpaRepository<PushMallStoreCoupon, Integer>, JpaSpecificationExecutor {
}
