package co.pushmall.modules.activity.repository;

import co.pushmall.modules.activity.domain.PushMallStoreCouponUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author pushmall
 * @date 2019-11-10
 */
public interface PushMallStoreCouponUserRepository extends JpaRepository<PushMallStoreCouponUser, Integer>, JpaSpecificationExecutor {
}
