package co.pushmall.modules.activity.service.impl;

import co.pushmall.modules.activity.domain.PushMallStorePink;
import co.pushmall.modules.activity.repository.PushMallStorePinkRepository;
import co.pushmall.modules.activity.service.PushMallStoreCombinationService;
import co.pushmall.modules.activity.service.PushMallStorePinkService;
import co.pushmall.modules.activity.service.dto.PushMallStoreCombinationDTO;
import co.pushmall.modules.activity.service.dto.PushMallStorePinkDTO;
import co.pushmall.modules.activity.service.dto.PushMallStorePinkQueryCriteria;
import co.pushmall.modules.activity.service.mapper.PushMallStorePinkMapper;
import co.pushmall.modules.shop.service.PushMallUserService;
import co.pushmall.modules.shop.service.dto.PushMallUserDTO;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-11-18
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStorePinkServiceImpl implements PushMallStorePinkService {

    private final PushMallStorePinkRepository pushMallStorePinkRepository;

    private final PushMallStoreCombinationService combinationService;
    private final PushMallUserService userService;

    private final PushMallStorePinkMapper pushMallStorePinkMapper;

    public PushMallStorePinkServiceImpl(PushMallStorePinkRepository pushMallStorePinkRepository, PushMallStoreCombinationService combinationService,
                                        PushMallUserService userService, PushMallStorePinkMapper pushMallStorePinkMapper) {
        this.pushMallStorePinkRepository = pushMallStorePinkRepository;
        this.combinationService = combinationService;
        this.userService = userService;
        this.pushMallStorePinkMapper = pushMallStorePinkMapper;
    }

    /**
     * 参与拼团的人
     *
     * @param id id
     * @return
     */
    @Override
    public int countPeople(int id) {
        return pushMallStorePinkRepository.countByKId(id) + 1;
    }

    @Override
    public Map<String, Object> queryAll(PushMallStorePinkQueryCriteria criteria, Pageable pageable) {
        criteria.setKId(0);
        Page<PushMallStorePink> page = pushMallStorePinkRepository
                .findAll((root, criteriaQuery, criteriaBuilder)
                        -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        List<PushMallStorePinkDTO> storePinkDTOS = pushMallStorePinkMapper.toDto(page.getContent());
        for (PushMallStorePinkDTO storePinkDTO : storePinkDTOS) {
            PushMallStoreCombinationDTO combinationDTO = combinationService
                    .findById(storePinkDTO.getCid());
            PushMallUserDTO userDTO = userService.findById(storePinkDTO.getUid());

            storePinkDTO.setAvatar(userDTO.getAvatar());
            storePinkDTO.setNickname(userDTO.getNickname());
            storePinkDTO.setTitle(combinationDTO.getTitle());
            storePinkDTO.setCountPeople(countPeople(storePinkDTO.getId()));
        }
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", storePinkDTOS);
        map.put("totalElements", page.getTotalElements());

        return map;
    }

    @Override
    public List<PushMallStorePinkDTO> queryAll(PushMallStorePinkQueryCriteria criteria) {
        return pushMallStorePinkMapper.toDto(pushMallStorePinkRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStorePinkDTO findById(Integer id) {
        Optional<PushMallStorePink> yxStorePink = pushMallStorePinkRepository.findById(id);
        ValidationUtil.isNull(yxStorePink, "PushMallStorePink", "id", id);
        return pushMallStorePinkMapper.toDto(yxStorePink.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStorePinkDTO create(PushMallStorePink resources) {
        return pushMallStorePinkMapper.toDto(pushMallStorePinkRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStorePink resources) {
        Optional<PushMallStorePink> optionalPushMallStorePink = pushMallStorePinkRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStorePink, "PushMallStorePink", "id", resources.getId());
        PushMallStorePink pushMallStorePink = optionalPushMallStorePink.get();
        pushMallStorePink.copy(resources);
        pushMallStorePinkRepository.save(pushMallStorePink);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallStorePinkRepository.deleteById(id);
    }
}
