package co.pushmall.modules.activity.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.activity.domain.PushMallStoreSeckill;
import co.pushmall.modules.activity.service.dto.PushMallStoreSeckillDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-12-14
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallStoreSeckillMapper extends EntityMapper<PushMallStoreSeckillDTO, PushMallStoreSeckill> {

}
